import sys
import psycopg2
import linecache

from ..DatabaseConnector import DatabaseConnector

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


class PostgresqlDatabaseConnector(DatabaseConnector):
    def __del__(self):
        self.disconnect()

    def executeRequest(self, request, parameters=None):
        try:
            cursor = self.connection_.cursor()
            cursor.execute(request, parameters)
            return cursor
        except psycopg2.OperationalError as err:
            self.disconnect()
            self.connect()
            cursor = self.connection_.cursor()
            cursor.execute(request, parameters)
            return cursor
        except psycopg2.InternalError as err:
            self.disconnect()
            self.connect()
            cursor = self.connection_.cursor()
            cursor.execute(request, parameters)
            return cursor

    def commit(self):
        try:
            self.connection_.commit()
        except psycopg2.OperationalError as err:
            self.disconnect()
            self.connect()
            self.connection_.commit()
        except psycopg2.InternalError as err:
            self.disconnect()
            self.connect()
            self.connection_.commit()

    def createDatabase(self):
        request = "CREATE DATABASE " + self.dbname_ + ";"
        self.disconnect()
        self.connect_(connectToDatabase=False)
        self.executeRequest(request, parameters=None)
        self.disconnect()

        grantRequest = "GRANT ALL ON SCHEMA public TO " + self.login_ + ";" \
                       "GRANT ALL ON SCHEMA public TO public;"

        self.connect()
        self.executeRequest(grantRequest)
        self.disconnect()

    def dropDatabase(self):
        request = "DROP DATABASE " + self.dbname_ + ";"
        self.disconnect()
        self.connect_(connectToDatabase=False)
        self.executeRequest(request, parameters=None)
        self.disconnect()

    def connect(self):
        self.connect_(connectToDatabase=True)

    def reconnectIfNecessary(self):
        connected = self.isConnected()
        if connected == False:
            self.disconnect()
            self.connect()

    def disconnect(self):
        if self.connection_ is None:
            return
        try:
            self.connection_.close()
            del self.connection_
        except:
            PrintException()

    def isConnected(self):
        if self.connection_ is None:
            return False
        try:
            self.connection_.isolation_level
            return True
        except psycopg2.OperationalError as exc:
            return False

# PRIVATE FUNCTIONS:

    def connect_(self, connectToDatabase=True):
        if connectToDatabase == True:
            self.connection_ = psycopg2.connect(dbname=self.dbname_,
                                                user=self.login_,
                                                password=self.password_,
                                                host=self.host_,
                                                port=self.port_)
        else:
            self.connection_ = psycopg2.connect(user=self.login_,
                                                password=self.password_,
                                                host=self.host_,
                                                port=self.port_)
        return

# FIELDS:

    connection_ = None
